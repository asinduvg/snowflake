CREATE DATABASE "EXERCISE_DB";

CREATE TABLE "EXERCISE_DB"."PUBLIC"."CUSTOMERS" (
    "id" INT,
    "first_name" VARCHAR,
    "last_name" VARCHAR,
    "email" VARCHAR,
    "age" INT,
    "city" VARCHAR
);

USE DATABASE "EXERCISE_DB";

USE WAREHOUSE COMPUTE_WH;

COPY INTO CUSTOMERS
FROM s3://snowflake-assignments-mc/gettingstarted/customers.csv
file_format=(type=csv
             field_delimiter=','
             skip_header=1);

SELECT COUNT(*) AS COUNT FROM CUSTOMERS;
             
