CREATE OR REPLACE STAGE EXERCISE_DB.PUBLIC.aws_stage
    url='s3://snowflake-assignments-mc/loadingdata';

LIST @EXERCISE_DB.PUBLIC.aws_stage;

COPY INTO EXERCISE_DB.PUBLIC.CUSTOMERS
    FROM @EXERCISE_DB.PUBLIC.aws_stage
    file_format = (type=csv field_delimiter=';' skip_header=1)
    pattern='.*customers.*csv';

SELECT * FROM EXERCISE_DB.PUBLIC.CUSTOMERS;
