-- Prepare database & table
CREATE OR REPLACE DATABASE COPY_DB;

CREATE OR REPLACE TABLE COPY_DB.PUBLIC.ORDERS (
    ORDER_ID VARCHAR(30),
    AMOUNT VARCHAR(30),
    PROFIT INT,
    QUANTITY INT,
    CATEGORY VARCHAR(30),
    SUBCATEGORY VARCHAR(30)
);

-- Prepare stage object
CREATE OR REPLACE STAGE COPY_DB.PUBLIC.AWS_STAGE_COPY
url='s3://snowflakebucket-copyoption/size/';

-- List files in the stage
LIST @COPY_DB.PUBLIC.AWS_STAGE_COPY;

USE WAREHOUSE COMPUTE_WH;

-- Load data using copy command
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*';

-- Not possible to load file that have been loaded and data has not been modified
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*';

-- Using the FORCE option
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*'
FORCE=TRUE;

