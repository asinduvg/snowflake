USE WAREHOUSE COMPUTE_WH;

-- CREATE NEW STAGE
CREATE OR REPLACE STAGE MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
url='s3://bucketsnowflakes4';

-- List files in the stage
LIST @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX;

-- Create example table
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID VARCHAR(30),
    AMOUNT INT,
    PROFIT INT,
    QUANTITY INT,
    CATEGORY VARCHAR(30),
    SUBCATEGORY VARCHAR(30)
);

-- Demonstrating error message
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv');

-- Validating table is empty
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Error handling using the ON_ERROR option
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv')
ON_ERROR='CONTINUE';

-- Validating results and truncating table
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;
SELECT COUNT(*) FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

TRUNCATE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Error handling using the ON_ERROR option=ABORT_STATEMENT (default)
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv', 'OrderDetails_error2.csv')
ON_ERROR='ABORT_STATEMENT';

-- Validating results and truncating table
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;
SELECT COUNT(*) FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

TRUNCATE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Error handling using the ON_ERROR option=SKIP_FILE
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv', 'OrderDetails_error2.csv')
ON_ERROR='SKIP_FILE';

-- Validating results and truncating table
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;
SELECT COUNT(*) FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

TRUNCATE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Error handling using the ON_ERROR option=SKIP_FILE_<number>
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv', 'OrderDetails_error2.csv')
ON_ERROR='SKIP_FILE_3';

-- Validating results and truncating table
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;
SELECT COUNT(*) FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

TRUNCATE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Error handling using the ON_ERROR option=SKIP_FILE_<number> | percentage
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE_ERROREX
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails_error.csv', 'OrderDetails_error2.csv')
ON_ERROR='SKIP_FILE_3%';

-- Validating results and truncating table
SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;
SELECT COUNT(*) FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

TRUNCATE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;
