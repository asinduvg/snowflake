CREATE OR REPLACE WAREHOUSE COMPUTE_WAREHOUSE
WITH
WAREHOUSE_SIZE=XSMALL
MAX_CLUSTER_COUNT=3
AUTO_SUSPEND=300
AUTO_RESUME=TRUE
INITIALLY_SUSPENDED=TRUE
COMMENT='This is our second warehouse'

SELECT * FROM CUSTOMER

DROP WAREHOUSE COMPUTE_WAREHOUSE

SELECT * FROM FIRST_DB.FIRST_SCHEMA.FIRST_FIRST;

DROP TABLE FIRST_FIRST;


-- Rename database & creating the table + meta data
ALTER DATABASE FIRST_DB RENAME TO OUR_FIRST_DB;
CREATE TABLE "OUR_FIRST_DB"."PUBLIC"."LOAN_PAYMENT" (
    "Loan_ID" STRING,
    "Loan_status" STRING,
    "Principal" STRING,
    "Terms" STRING,
    "Effective_date" STRING,
    "Due_date" STRING,
    "Paid_off_time" STRING,
    "Past_due_days" STRING,
    "Age" STRING,
    "Education" STRING,
    "Gender" STRING
);

-- Check that table is empty
USE DATABASE OUR_FIRST_DB;

SELECT * FROM LOAN_PAYMENT;

-- Loading the data from S3 bucket
COPY INTO LOAN_PAYMENT
FROM s3://bucketsnowflakes3/Loan_payments_data.csv
file_format=(type=csv
             field_delimiter=','
             skip_header=1);

-- Validate
SELECT * FROM LOAN_PAYMENT;

