-- Transforming using the SELECT statement

USE WAREHOUSE COMPUTE_WH;

-- Example 1 - Table
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID VARCHAR(30),
    AMOUNT INT
);

LIST @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE;

COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM (SELECT s.$1, s.$2 FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE s)
file_format=(type=csv field_delimiter=',' skip_header=1)
files=('OrderDetails.csv');

SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Example 2 - Table
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID VARCHAR(30),
    AMOUNT INT,
    PROFIT INT,
    PROFITABLE_FLAG VARCHAR(30)
);

-- Example 2 - Copy Command using a SQL function (subset of the functions available)

COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM (SELECT s.$1, s.$2, s.$3,
      CASE WHEN CAST(s.$3 AS INT) < 0 THEN 'not profitable' ELSE 'profitable' END
      FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE s)
      file_format=(type=csv field_delimiter=',' skip_header=1)
      files=('OrderDetails.csv');

SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Example 3 - Table
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID VARCHAR(30),
    AMOUNT INT,
    PROFIT INT,
    CATEGORY_SUBSTRING VARCHAR(5)
);

-- Example 3 - Copy Command using a SQL function (subset of functions available)
COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX
FROM (SELECT s.$1, s.$2, s.$3, SUBSTRING(s.$5, 1, 5)
      FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE s
      )
      file_format=(type=csv field_delimiter=',' skip_header=1)
      files=('OrderDetails.csv');

SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;

-- Example 4 - Using subset of columns
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID VARCHAR(30),
    AMOUNT INT,
    PROFIT INT,
    CATEGORY_SUBSTRING VARCHAR(5)
);

COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX (ORDER_ID, PROFIT)
FROM (SELECT s.$1, s.$3
      FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE s)
      file_format=(type=csv field_delimiter=',' skip_header=1)
      files=('OrderDetails.csv');

SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX;      

-- Example 5 - Table Auto increment
CREATE OR REPLACE TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX (
    ORDER_ID NUMBER AUTOINCREMENT START 1 INCREMENT 1,
    AMOUNT INT,
    PROFIT INT,
    PROFITABLE_FLAG VARCHAR(30)
);

COPY INTO OUR_FIRST_DB.PUBLIC.ORDERS_EX (PROFIT, AMOUNT)
FROM (SELECT s.$2, s.$3
      FROM @MANAGE_DB.EXTERNAL_STAGES.AWS_STAGE s)
      file_format=(type=csv field_delimiter=',' skip_header=1)
      files=('OrderDetails.csv');

SELECT * FROM OUR_FIRST_DB.PUBLIC.ORDERS_EX WHERE ORDER_ID > 15;  

DROP TABLE OUR_FIRST_DB.PUBLIC.ORDERS_EX;
