-- Prepare database & table
CREATE OR REPLACE DATABASE COPY_DB;

CREATE OR REPLACE TABLE COPY_DB.PUBLIC.ORDERS (
    ORDER_ID VARCHAR(30),
    AMOUNT VARCHAR(30),
    PROFIT INT,
    QUANTITY INT,
    CATEGORY VARCHAR(30),
    SUBCATEGORY VARCHAR(30)
);

-- Prepare stage object
CREATE OR REPLACE STAGE COPY_DB.PUBLIC.AWS_STAGE_COPY
url='s3://snowflakebucket-copyoption/size/';

LIST @COPY_DB.PUBLIC.AWS_STAGE_COPY;

USE WAREHOUSE COMPUTE_WH;

-- Load data using copy command
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*'
VALIDATION_MODE=RETURN_ERRORS;

SELECT * FROM COPY_DB.PUBLIC.ORDERS;

COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*'
VALIDATION_MODE=RETURN_5_ROWS;

-- Use files with errors
CREATE OR REPLACE STAGE COPY_DB.PUBLIC.AWS_STAGE_COPY
url='s3://snowflakebucket-copyoption/returnfailed/';

LIST @COPY_DB.PUBLIC.AWS_STAGE_COPY;

-- Show all errors
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*Order.*'
VALIDATION_MODE=RETURN_ERRORS;

-- Validate first n rows
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern='.*error.*'
VALIDATION_MODE=RETURN_5_ROWS;

-- Assignment 5
CREATE OR REPLACE TABLE COPY_DB.PUBLIC.EMPLOYEES(
    CUSTOMER_ID INT,
    FIRST_NAME VARCHAR(50),
    LAST_NAME VARCHAR(50),
    EMAIL VARCHAR(50),
    AGE INT,
    DEPARTMENT VARCHAR(50)
);

CREATE OR REPLACE STAGE COPY_DB.PUBLIC.AWS_STAGE_COPY
url='s3://snowflake-assignments-mc/copyoptions/example1';

LIST @COPY_DB.PUBLIC.AWS_STAGE_COPY;

CREATE OR REPLACE FILE FORMAT COPY_DB.PUBLIC.FILE_FORMAT
TYPE=CSV
FIELD_DELIMITER=','
SKIP_HEADER=1;

COPY INTO COPY_DB.PUBLIC.EMPLOYEES
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=COPY_DB.PUBLIC.FILE_FORMAT
VALIDATION_MODE=RETURN_ERRORS;

COPY INTO COPY_DB.PUBLIC.EMPLOYEES
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=COPY_DB.PUBLIC.FILE_FORMAT
ON_ERROR=CONTINUE;

SELECT COUNT(*) FROM COPY_DB.PUBLIC.EMPLOYEES;

-- Working with error results
-- 1. Saving rejected files after VALIDATION_MODE

CREATE OR REPLACE TABLE COPY_DB.PUBLIC.ORDERS (
    ORDER_ID VARCHAR(30),
    AMOUNT VARCHAR(30),
    PROFIT INT,
    QUANTITY INT,
    CATEGORY VARCHAR(30),
    SUBCATEGORY VARCHAR(30)
);

CREATE OR REPLACE STAGE COPY_DB.PUBLIC.AWS_STAGE_COPY
url='s3://snowflakebucket-copyoption/returnfailed/';

LIST @COPY_DB.PUBLIC.AWS_STAGE_COPY;

COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
FILE_FORMAT=(TYPE=CSV FIELD_DELIMITER=',' SKIP_HEADER=1)
PATTERN='.*Order.*'
VALIDATION_MODE=RETURN_ERRORS;

-- Storing rejected/failed results in a table
CREATE OR REPLACE TABLE REJECTED AS
SELECT REJECTED_RECORD FROM TABLE(RESULT_SCAN(LAST_QUERY_ID()));

SELECT * FROM COPY_DB.PUBLIC.REJECTED;

CREATE OR REPLACE TABLE REJECTED AS
SELECT REJECTED_RECORD FROM TABLE(RESULT_SCAN('01b21b23-0000-8d44-0000-00067be2bded'));

-- Adding additional records
INSERT INTO COPY_DB.PUBLIC.REJECTED
SELECT REJECT_RECORD FROM TABLE(RESULT_SCAN(LAST_QUERY_ID()));

-- 2. Saving rejected files without VALIDATION_MODE
COPY INTO COPY_DB.PUBLIC.ORDERS
FROM @COPY_DB.PUBLIC.AWS_STAGE_COPY
file_format=(type=csv field_delimiter=',' skip_header=1)
pattern=".*Order.*"
ON_ERROR=CONTINUE;

SELECT * FROM TABLE(VALIDATE(ORDERS, JOB_ID => '_last'));

-- 3. Working with rejected records
SELECT REJECTED_RECORD FROM COPY_DB.PUBLIC.REJECTED;

CREATE OR REPLACE TABLE COPY_DB.PUBLIC.REJECTED_VALUES AS
SELECT
SPLIT_PART(REJECTED_RECORD, ',', 1) AS ORDER_ID,
SPLIT_PART(REJECTED_RECORD, ',', 2) AS AMOUNT,
SPLIT_PART(REJECTED_RECORD, ',', 3) AS PROFIT,
SPLIT_PART(REJECTED_RECORD, ',', 4) AS QUANTITY,
SPLIT_PART(REJECTED_RECORD, ',', 5) AS CATEGORY,
SPLIT_PART(REJECTED_RECORD, ',', 6) AS SUBCATEGORY
FROM REJECTED;

SELECT * FROM COPY_DB.PUBLIC.REJECTED_VALUES;



